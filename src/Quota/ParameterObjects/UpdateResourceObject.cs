﻿using System;

namespace Coscine.Api.Quota.ParameterObjects
{
    /// <summary>
    ///  Parameter object containing the update informations.
    /// </summary>    
    public class UpdateResourceObject
    {
        /// <summary>
        ///  Id of the resource.
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// New Quota value.
        /// </summary>
        public int Allocated { get; set; }
    }
}
