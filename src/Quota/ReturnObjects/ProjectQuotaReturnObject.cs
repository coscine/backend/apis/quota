﻿using System;

namespace Coscine.Api.Quota.ReturnObjects
{
    /// <summary>
    /// Contains information about the quota of a resource.
    /// </summary>    
    public class ProjectQuotaReturnObject
    {
        /// <summary>
        /// Id of the resoure.
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// Display name of the resource.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// How much space is used by the resource (rounded up and in gb).
        /// </summary>
        public int Used { get; set; }
        /// <summary>
        /// How much space is availabe to be taken by resource (in gb).
        /// </summary>
        public int Allocated { get; set; }
    }
}
