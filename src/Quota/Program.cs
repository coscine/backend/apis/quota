﻿using Coscine.ApiCommons;
using Coscine.Configuration;

namespace Coscine.Api.Quota
{
    public class Program : AbstractProgram<ConsulConfiguration>
    {
        /// <summary>
        /// Standard Main method.
        /// </summary>
        /// <param name="args"></param>
        static void Main()
        {
            InitializeWebService<Startup>();
        }

    }
}
