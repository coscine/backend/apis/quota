﻿using Coscine.Api.Quota.ParameterObjects;
using Coscine.Api.Quota.ReturnObjects;
using Coscine.ApiCommons;
using Coscine.Configuration;
using Coscine.Database.DataModel;
using Coscine.Database.Models;
using Coscine.Database.Util;
using Coscine.Logging;
using Coscine.ResourceTypes;
using Coscine.ResourceTypes.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Coscine.Api.Quota.Controllers
{
    /// <summary>
    /// Controller for quotas.
    /// </summary>
    [Authorize]
    public class QuotaController : Controller
    {
        private readonly Authenticator _authenticator;

        private readonly CoscineLogger _coscineLogger;
        private readonly ResourceModel _resourceModel;
        private readonly ProjectModel _projectModel;
        private readonly ProjectRoleModel _projectRoleModel;
        private readonly ProjectQuotaModel _projectQuotaModel;
        private readonly ProjectResourceModel _projectResourceModel;
        private readonly ResourceTypeModel _resourceTypeModel;
        private readonly float _oneGb = 1024 * 1024 * 1024;

        /// <summary>
        /// Default constructor with initialization.
        /// </summary>
        public QuotaController(ILogger<QuotaController> logger)
        {
            _authenticator = new Authenticator(this, Program.Configuration);

            _coscineLogger = new CoscineLogger(logger);
            _resourceModel = new ResourceModel();
            _projectModel = new ProjectModel();
            _projectRoleModel = new ProjectRoleModel();
            _projectQuotaModel = new ProjectQuotaModel();
            _projectResourceModel = new ProjectResourceModel();
            _resourceTypeModel = new ResourceTypeModel();
        }

        /// <summary>
        /// Build return object from resource informations.
        /// </summary>
        /// <param name="resource">The used resource.</param>
        /// <param name="resourceTypeDefinition">The used ResourceTypeDefinition.</param>
        /// <returns>A list of the project quotas objects.</returns>
        private ProjectQuotaReturnObject CreateProjectQuotaReturnObject(Resource resource, BaseResourceType resourceTypeDefinition)
        {
            return new ProjectQuotaReturnObject
            {
                Id = resource.Id,
                Name = resource.DisplayName,
                // _oneGb is float -> float division
                Used = (int)Math.Ceiling(resourceTypeDefinition.GetResourceQuotaUsed(resource.Id.ToString()).Result / _oneGb),
                Allocated = (int)resourceTypeDefinition.GetResourceQuotaAvailable(resource.Id.ToString()).Result,
            };
        }

        private int CalculateAllocatedForAll(ResourceType resourceType, Guid projectId)
        {
            var resources = _resourceModel.GetAllWhere((resource) =>
                        (from projectResource in resource.ProjectResources
                         where projectResource.ProjectId == projectId
                         select projectResource).Any() &&
                         resource.TypeId == resourceType.Id);

            var used = resources.Sum(resource => ResourceTypeFactory
                .Instance
                .GetResourceType(resource)
                .GetResourceQuotaAvailable(resource.Id.ToString())
                .Result);

            return (int)used;
        }

        /// <summary>
        /// Retrieves the quota for the selected project and resource type.
        /// </summary>
        /// <param name="projectId">Id of the project.</param>
        /// <param name="resourceTypeId">Id of the resource type.</param>
        /// <returns>A list of the project quotas objects.</returns>
        [HttpGet("[controller]/{projectId}/{resourceTypeId}/all")]
        public ActionResult<List<ProjectQuotaReturnObject>> GetResourceQuotas(string projectId, string resourceTypeId)
        {
            if (!Guid.TryParse(projectId, out Guid projectGuid))
            {
                return BadRequest($"{projectId} is not a GUID.");
            }

            var project = _projectModel.GetById(projectGuid);

            if (project == null)
            {
                return NotFound($"Could not find project with id: {projectId}");
            }

            if (!_projectModel.HasAccess(_authenticator.GetUser(), project, UserRoles.Owner))
            {
                return Unauthorized("The user is not authorized to perform a get on the selected project!");
            }

            if (!Guid.TryParse(resourceTypeId, out Guid resourceTypeGuid))
            {
                return BadRequest($"{resourceTypeId} is not a GUID.");
            }

            var displayName = _resourceTypeModel.GetById(resourceTypeGuid)?.DisplayName;

            if (string.IsNullOrWhiteSpace(displayName))
            {
                return NotFound($"Could not find resourceType with id: {resourceTypeId}");
            }

            var resources = _projectResourceModel.GetAllWhere(x => x.ProjectId == projectGuid).Select(x => _resourceModel.GetById(x.ResourceId)).Where(x => x.TypeId == resourceTypeGuid);

            if (!resources.Any())
            {
                return Json(new List<ProjectQuotaReturnObject>());
            }

            return Json(
                resources.Select(resource =>
                {
                    var resourceTypeDefinition = ResourceTypeFactory.Instance.GetResourceType(resource);
                    return CreateProjectQuotaReturnObject(resource, resourceTypeDefinition);
                })
                .ToList()
            );
        }

        /// <summary>
        /// Retrieves the quota for the resource.
        /// </summary>
        /// <param name="resourceId">Id of the resource.</param>
        /// <returns>The quota of the resource.</returns>
        [HttpGet("[controller]/{resourceId}")]
        public ActionResult<ProjectQuotaReturnObject> GetResourceQuota(string resourceId)
        {
            if (!Guid.TryParse(resourceId, out Guid resourceGuid))
            {
                return BadRequest($"{resourceId} is not a GUID.");
            }

            var resource = _resourceModel.GetById(resourceGuid);

            if (resource == null)
            {
                return NotFound($"Could not find resource with id: {resourceId}");
            }

            var projectId = _projectResourceModel.GetWhere(x => x.ResourceId == resourceGuid).ProjectId;

            if (!_projectModel.HasAccess(_authenticator.GetUser(), projectId, UserRoles.Owner))
            {
                return Unauthorized("The user is not authorized to perform a get on the selected project!");
            }

            var displayName = _resourceTypeModel.GetById(resource.TypeId).DisplayName;

            var resourceTypeDefinition = ResourceTypeFactory.Instance.GetResourceType(resource);

            if (resourceTypeDefinition == null)
            {
                return BadRequest($"No provider for: \"{displayName}\".");
            }

            return Json(CreateProjectQuotaReturnObject(resource, resourceTypeDefinition));
        }

        /// <summary>
        /// Updates the quota for a resource.
        /// </summary>
        /// <param name="updateResourceObject">Contains the resource id and the new quota.</param>
        /// <returns>204 if successful.</returns>
        [HttpPost("[controller]/{resourceId}")]
        public IActionResult UpdateResourceQuota([FromBody] UpdateResourceObject updateResourceObject)
        {
            var resource = _resourceModel.GetById(updateResourceObject.Id);

            if (resource == null)
            {
                return NotFound($"Could not find resource with id: {updateResourceObject.Id}");
            }

            var projectId = _projectResourceModel.GetWhere(x => x.ResourceId == updateResourceObject.Id).ProjectId;
            var user = _authenticator.GetUser();

            if (!_projectModel.HasAccess(user, projectId, UserRoles.Owner))
            {
                return Unauthorized("The user is not authorized to perform a get on the selected project!");
            }

            var resourceType = _resourceTypeModel.GetById(resource.TypeId);

            if (resourceType == null)
            {
                return NotFound($"Could not find resourceType with id: {resource.TypeId}");
            }

            var resourceTypeDefinition = ResourceTypeFactory.Instance.GetResourceType(resource);

            if (resourceTypeDefinition == null)
            {
                return BadRequest($"No provider for: \"{resource.Type.DisplayName}\".");
            }

            if (updateResourceObject.Allocated < 1)
            {
                return BadRequest($"Allocated {updateResourceObject.Allocated}. Cannot be less than 1.");
            }

            var projectQuota =
                _projectQuotaModel.GetWhere((x) =>
                    x.ProjectId == projectId &&
                    x.ResourceTypeId == resource.TypeId);

            var allocatedForAll = CalculateAllocatedForAll(resourceType, projectId);

            var allocatedForCurrent = resourceTypeDefinition.GetResourceQuotaAvailable(resource.Id.ToString()).Result;

            if (allocatedForAll - allocatedForCurrent + updateResourceObject.Allocated > projectQuota.Quota)
            {
                return BadRequest($"Cannot set quota to {updateResourceObject.Allocated} for resource {updateResourceObject.Id}. Currently {allocatedForAll} GB of {projectQuota.Quota} GB are allocated. You would add {updateResourceObject.Allocated - allocatedForCurrent} GB.");
            }

            var used = (int)Math.Ceiling(resourceTypeDefinition.GetResourceQuotaUsed(resource.Id.ToString()).Result / _oneGb);

            if (updateResourceObject.Allocated < used)
            {
                return BadRequest($"Cannot set quota to {updateResourceObject.Allocated} for resource {updateResourceObject.Id}. Currently {used} GB of {allocatedForCurrent} GB are stored. You cannot go below the already stored size.");
            }

            resourceTypeDefinition.SetResourceQuota(resource.Id.ToString(), updateResourceObject.Allocated).Wait();

            if (IsLikeRds(resourceType.DisplayName))
            {
                var rdsResourceTypeModel = new RDSResourceTypeModel();
                var rdsResourceType = rdsResourceTypeModel.GetById(resource.ResourceTypeOptionId.Value);
                rdsResourceType.Size = updateResourceObject.Allocated;
                rdsResourceTypeModel.Update(rdsResourceType);
            }
            else if (IsLikeRdsS3(resourceType.DisplayName))
            {
                var rdsS3ResourceTypeModel = new RdsS3ResourceTypeModel();
                var rdsS3ResourceType = rdsS3ResourceTypeModel.GetById(resource.ResourceTypeOptionId.Value);
                rdsS3ResourceType.Size = updateResourceObject.Allocated;
                rdsS3ResourceTypeModel.Update(rdsS3ResourceType);
            }

            if (Request.Query != null && Request.Query["noanalyticslog"] != "true")
            {
                LogAnalyticsOwnerResourceQuotaChange(resource, user, used, updateResourceObject.Allocated);
            }

            return NoContent();
        }

        private static bool IsLikeRds(string compare)
        {
            return compare == "rds" || compare == "rdsude" || compare == "rdsnrw" || compare == "rdstudo";
        }

        private static bool IsLikeRdsS3(string compare)
        {
            return compare == "rdss3" || compare == "rdss3ude" || compare == "rdss3nrw" || compare == "rdss3tudo";
        }

        private void LogAnalyticsOwnerResourceQuotaChange(Resource resource, User user, int used, int allocated)
        {
            var resourceTypeDisplayName = _resourceTypeModel.GetById(resource.TypeId).DisplayName;
            var projectId = _projectResourceModel.GetProjectForResource(resource.Id).Value;

            _coscineLogger.AnalyticsLog(
                new AnalyticsLogObject
                {
                    Type = "Action",
                    Operation = "Owner Resource Quota Change",
                    RoleId = _projectRoleModel.GetGetUserRoleForProject(projectId, user.Id).ToString(),
                    ProjectId = projectId.ToString(),
                    QuotaSize = new List<string> { $"{resourceTypeDisplayName}: {used}/{allocated}" },
                    ResourceId = resource.Id.ToString(),
                    ApplicationsProfile = resource.ApplicationProfile
                });
        }
    }
}